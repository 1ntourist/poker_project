import React from 'react';
import './App.css';
import {Component} from "react";
import Card from "./components/Card/Card";
import {AllCards} from './dependencies/cardDeck'
import {PokerHand} from "./dependencies/pokerHand";


const testArr =
    [{value: 14, id: 11, suit: "diams", rank: "a"},
    {value: 2, id: 18, suit: "hearts", rank: "2"},
    {value: 4, id: 2, suit: "diams", rank: "4"},
    {value: 3, id: 30, suit: "spades", rank: "3"},
    {value: 5, id: 31, suit: "spades", rank: "5"}]


class App extends Component {
    constructor() {
        super();
        this.instance = new AllCards;
        this.state = {
            yourDeck: this.instance.cards,
            yourCards: this.instance.getCards(5)
        }
    }

    distributeNewCards = () => {
        const newInstance = new AllCards;
        const newDeck = newInstance.cards;
        const newYourCards = newInstance.getCards(5);
        this.setState({
            yourDeck: newDeck,
            yourCards: newYourCards,
        });
        return newYourCards;
    }


    pokerHand() {
        const pokerHand = new PokerHand(this.state.yourCards);
        const value = pokerHand.getOutCome();
        return value;
    }


    render() {
        return (
        <>
            <div className="App playingCards simpleCards">
                {this.state.yourCards.map(card => (
                    <Card
                        key={card.id}
                        suit={card.suit}
                        rank={card.rank}/>
                ))}
            </div>
            <p>{this.pokerHand()}</p>
            <button onClick={this.distributeNewCards}>Distribute new cards</button>
        </>
    )
    }

}

export  default App;