export class PokerHand {
    constructor(arr) {
        this.cards = arr;
    }

    getOutCome() {
        let value;
        const onePair = 'One pair';
        const twoPair = 'Two pair';
        const threeCards = 'Three of a kind';
        const care = 'Four of a kind';
        const flush = 'Flush';
        const fullHouse = 'Full house';


        let cardsRank = [];
        let cardsSuit = [];
        let cardsValue = [];
        this.cards.forEach(obj => {
            cardsRank.push(obj['rank']);
            cardsSuit.push(obj['suit']);
            cardsValue.push(obj['value']);
        })

        cardsValue.sort(function(a, b) {
            return a - b;
        });


        let newArray = cardsRank.filter((item, index) => cardsRank.indexOf(item) !== index);
        let setArray = new Set(cardsRank);


        // Dont work for Street Flash
        const street = () => {
            let flag = false;
            let flagArr = [];
            let ace = false;

            for (let i=0; i< cardsValue.length; i++) {
                ace = cardsValue[i] === 14;
            }

            console.log(cardsValue)
            console.log(cardsValue.reverse())

            for (let i =1; i < 5; i++) {
                if (cardsValue[i+1]) {
                    if ((cardsValue.reverse()[i] - cardsValue.reverse()[i+1]) === 1) {
                        flagArr.push(1)
                    } else {
                        flagArr.push(2)
                    }
                }
            }

            console.log(flagArr)

            if (Array.from(new Set(flagArr)) === 1  && ace) {
                flag = true
            }

            return flag
        }


        if (newArray.length === 1 && Array.from(setArray).length === 4) {
            value = onePair
        } else if (newArray.length === 2 && Array.from(setArray).length === 3 && newArray[0] !== newArray[1]) {
            value = twoPair
        } else if (newArray.length === 2 && Array.from(setArray).length === 3 && newArray[0] === newArray[1]) {
            value = threeCards
        } else if (newArray.length === 3 && Array.from(setArray).length === 2 && Array.from(new Set(newArray)).length === 1 ){
            value = care
        } else if (newArray.length === 3 && Array.from(setArray).length === 2 && Array.from(new Set(newArray)).length === 2 ){
            value = fullHouse
        } else if (Array.from(new Set(cardsSuit)) === 1) {
            value = flush;
        } else {
            value = 'High card'
        }

        return value;
    }


}


