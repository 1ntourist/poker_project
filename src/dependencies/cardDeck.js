export class AllCards {
    constructor() {
        this.cards = this.__createDeck();
    }

    __createDeck() {
        const suits = ['diams', 'hearts', 'spades', 'clubs' ]
        let mainArr = [];
        let index = 0;
        for (let i=0; i < 4; i++) {
            let value = 2;
            let rank = ['2', '3', '4',
                        '5', '6', '7',
                        '8', '9', '10',
                        'j', 'q', 'k', 'a'];
            for (let y=0; y < 13; y++) {
                let card = {
                    value: value ++,
                    id: index++,
                    suit: suits[i],
                    rank: rank[y]
                }
                mainArr.push(card);
            }

        }
        return mainArr;
    }

    getCard() {
        let card = this.cards.splice(Math.floor(Math.random() * this.cards.length), 1);
        return card[0];

    }

    getCards(value) {
        let cards = [];
        for (let i = 0; i < value; i++) {
            let newCard = this.getCard()
            cards.push(newCard);
        }
        return cards;
    }
}