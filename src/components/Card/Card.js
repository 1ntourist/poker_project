import React from 'react';
import './css/cards.css'

const getSymbol = value => {
    value = `&${value};`;
    return (
        <span className="suit" dangerouslySetInnerHTML={{__html: value}}></span>
        )
}

const Card = props => {
    const newClassName = `card rank-${props.rank} ${props.suit}`
    const symbol = '&' + `${props.suit}` + ';'
    return (
        <div className={newClassName}>
            <span className="rank">{props.rank.toUpperCase()}</span>
            {getSymbol(props.suit)}
        </div>
    );
};

export default Card;